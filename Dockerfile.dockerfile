FROM python:3.9

LABEL maintainer="kheixmiang"

WORKDIR /code

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

RUN apt-get update && apt -y install iputils-ping

COPY scan /code
 
CMD ["python", "portscanning.py"]                                                                                                               
                                                                                                                                
                                                           